/**
 * Permission.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoPK: false,
  schema: true,

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      unique: true,
      autoIncrement: true,
    },
    name: {
      type: 'string',
    },
    roles: {
      collection: 'role',
      via: 'permissions',
    },
  },
  beforeCreate: function(values, next) {

    delete values.id;
    delete values.createdAt;
    delete values.updatedAt;
    next();
  },
};
