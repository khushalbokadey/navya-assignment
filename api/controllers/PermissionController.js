/**
 * PermissionController
 *
 * @description :: Server-side logic for managing Permissions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  getPermissions: function(req, res) {
    let userId = req.params.userId;

    let sqlQuery = 'SELECT p.name AS pname, p.id AS pid, u.id AS uid, u.name AS uname FROM permission p LEFT JOIN permission_roles__role_permissions prrp ON p.id = prrp.permission_roles LEFT JOIN role_users__user_roles ruur ON prrp.role_permissions = ruur.role_users LEFT JOIN user u ON ruur.user_roles = u.id WHERE u.id = ?';

    Permission.query(sqlQuery, [userId], function(err, result) {
      if (err) {
        return res.serverError(err);
      }
      if (result.length === 0) {
        return res.json({});
      }
      let output = {
        id: result[0].uid,
        name: result[0].uname,
        roles: []
      };
      _.each(result, function(row) {
        output.roles.push(row.pname);
      });
      return res.json(output);
    });
  },

  checkPermission: function(req, res) {
    let permissionid = req.query.permissionid;
    let userid = req.query.userid;

    let sqlQuery = 'SELECT p.name AS pname, p.id AS pid, u.id AS uid, u.name AS uname FROM permission p LEFT JOIN permission_roles__role_permissions prrp ON p.id = prrp.permission_roles LEFT JOIN role_users__user_roles ruur ON prrp.role_permissions = ruur.role_users LEFT JOIN user u ON ruur.user_roles = u.id WHERE u.id = ? AND p.id = ?';

    User.query(sqlQuery, [userid, permissionid], function(err, result) {
      if (err) {
        return res.serverError(err);
      }
      if (result.length === 0) {
        return res.forbidden();
      } else {
        return res.ok();
      }
    });
  },

  modifyPermission: function(req, res) {
    let roleId = req.params.roleId;
    let permissions = req.body.permissions;

    async.auto({
      deleteRolePerms: function(asyncCB) {
        Role.query('DELETE FROM permission_roles__role_permissions WHERE role_permissions = ?', [roleId], asyncCB);
      },
      perms: function(asyncCB) {
        var query = 'SELECT id FROM permission WHERE name IN (';
        var valuesToEscape = [];
        _.map(permissions, function(p) {
          query += '?, ';
          valuesToEscape.push(p);
        });
        query = query.slice(0, query.length - 2);
        query += ')'
        Permission.query(query, valuesToEscape, asyncCB);
      },
      newPerms: ['deleteRolePerms', 'perms', function(asyncCB, results) {

        var query = 'INSERT INTO permission_roles__role_permissions (permission_roles, role_permissions) VALUES ';
        var valuesToEscape = [];

        _.map(results.perms, function(p) {
          query += '(?, ?), ';
          valuesToEscape.push(p.id);
          valuesToEscape.push(roleId);
        });
        query = query.slice(0, query.length - 2);

        Permission.query(query, valuesToEscape, asyncCB)
      }],
    }, function(err, result) {
      if (err) {
        return res.serverError(err);
      }
      return res.json(result.newPerms);
    });
  },

  deletePermission: function(req, res) {
    let pId = req.params.pId;

    async.auto({
      deleteAssociation: function(asyncCB) {
        Permission.query('DELETE FROM permission_roles__role_permissions WHERE permission_roles = ?', [pId], asyncCB);
      },
      deletePermission: ['deleteAssociation', function(asyncCB) {
        Permission.query('DELETE FROM permission WHERE id = ?', [pId], asyncCB);
      }]
    }, function(err, results) {
      if (err) {
        return res.serverError();
      }
      return res.ok();
    })
  },

};
