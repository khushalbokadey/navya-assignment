/** 
 * Test model: PermissionModel.test.js
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @sails docs  :: http://sailsjs.org/documentation/concepts/testing - 
 * @chai docs   :: http://chaijs.com/guide/styles/
 * @sinon docs  :: http://sinonjs.org/docs/
 * @supertest   :: https://github.com/visionmedia/supertest
 */
"use strict";

var chai = require('chai');
var assert = chai.assert;
var sinon = require('sinon');
var request = require('supertest');
var expect = chai.expect;

//TODO: you must create the defining test
var data = {};

describe('Controller:Permission', () => {

  //TODO: you must create the defining logic to get by id
  var id = 1;

  describe('GET /user/:id', () => {
    it('should respond with the requested User:id', done => {
      request(sails.hooks.http.app)
        .get(sails.config.blueprints.prefix + '/user/' + data.id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err)
            return done(err);

          expect(res.body.code)
            .to.equal('OK');
          done();
        });
    });
  });

  //Clear Permission after testing
  after(function() {
    return Permission.destroy();
  });
});
